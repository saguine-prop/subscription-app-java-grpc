package com.ardab.subsapp.subscription.service;

import com.ardab.gprc.mobilepackage.*;
import com.ardab.gprc.subscription.SubscriptionErrorCode;
import com.ardab.subsapp.subscription.exception.SubscriptionException;
import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.rpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.StatusProto;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;

@Service
public class PackageCheckService {

    @GrpcClient("mobilepackage")
    private PackageServiceGrpc.PackageServiceBlockingStub packageServiceBlockingStub;

    private PackageRequest packageRequest;
    private PackageResponse packageResponse;
    private PackageExceptionResponse exceptionResponse;

    public PackageResponse packageDetails(Integer packageId){
        return getPackageDetails(packageId);
    }

    private PackageResponse getPackageDetails(Integer packageId){
        packageRequest = PackageRequest.newBuilder()
                .setSubscriptionPackage(packageId)
                .build();
        try{
            packageResponse = packageServiceBlockingStub.getPackageInfo(packageRequest);
            return packageResponse;
        } catch (StatusRuntimeException exception){
            Status status = StatusProto.fromThrowable(exception);
            for (Any any : status.getDetailsList()) {
                if(!any.is(PackageExceptionResponse.class)){
                    continue;
                }
                try {
                    exceptionResponse = any.unpack(PackageExceptionResponse.class);
                    System.out.println("Error Code: " + exceptionResponse.getErrorCode());
                    if(exceptionResponse.getErrorCode() == MobilePackageErrorCode.INVALID_PACKAGE_CODE_VALUE){
                        throw new SubscriptionException(SubscriptionErrorCode.INVALID_PACKAGE_CODE_VALUE);
                    }
                    else if(exceptionResponse.getErrorCode() == MobilePackageErrorCode.NOT_ACTIVE_PACKAGE){
                        throw new SubscriptionException(SubscriptionErrorCode.NOT_ACTIVE_PACKAGE);
                    }
                } catch (InvalidProtocolBufferException e) {
                    e.printStackTrace();
                }
            }
        }
        return packageResponse;
    }
}
