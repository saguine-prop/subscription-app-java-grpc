package com.ardab.subsapp.subscription.exception;

import com.ardab.gprc.subscription.SubscriptionExceptionResponse;
import com.google.protobuf.Any;
import com.google.protobuf.Timestamp;
import com.google.rpc.Code;
import com.google.rpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.StatusProto;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;

import java.time.Instant;

@GrpcAdvice
public class SubscriptionExceptionHandler {

    @GrpcExceptionHandler(SubscriptionException.class)
    public StatusRuntimeException insufficientBalanceError(SubscriptionException exception){

        Instant time = Instant.now();
        Timestamp timestamp = Timestamp.newBuilder().setSeconds(time.getEpochSecond())
                .setNanos(time.getNano()).build();
        
        SubscriptionExceptionResponse subscriptionExceptionResponse = SubscriptionExceptionResponse.newBuilder()
                .setErrorCode(exception.getErrorCode())
                .setTimestamp(timestamp)
                .build();

        Status status = Status.newBuilder()
                .setCode(Code.INVALID_ARGUMENT.getNumber())
                .setMessage("Insufficent balance")
                .addDetails(Any.pack(subscriptionExceptionResponse))
                .build();
        return StatusProto.toStatusRuntimeException(status);
    }
}
