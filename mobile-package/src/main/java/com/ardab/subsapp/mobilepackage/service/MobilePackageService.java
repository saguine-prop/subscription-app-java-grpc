package com.ardab.subsapp.mobilepackage.service;

import com.ardab.gprc.mobilepackage.MobilePackageErrorCode;
import com.ardab.gprc.mobilepackage.PackageRequest;
import com.ardab.gprc.mobilepackage.PackageResponse;
import com.ardab.gprc.mobilepackage.PackageServiceGrpc;
import com.ardab.subsapp.mobilepackage.exception.PackageException;
import com.ardab.subsapp.mobilepackage.model.MobilePackage;
import com.ardab.subsapp.mobilepackage.repository.MobilePackageRepository;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;


@GrpcService
public class MobilePackageService extends PackageServiceGrpc.PackageServiceImplBase {

    @Autowired
    MobilePackageRepository mobilePackageRepository;

    @Autowired
    PackageValidationService packageValidationService;

    @Override
    public void getPackageInfo(PackageRequest request, StreamObserver<PackageResponse> responseObserver) {
        int packageId=request.getSubscriptionPackage();
        packageValidationService.validatePackageCode(packageId);

        MobilePackage mobilePackage = mobilePackageRepository.findById(packageId).get();

        PackageResponse response = PackageResponse.newBuilder()
                     .setPackagePrice(mobilePackage.getPackagePrice())
                     .setPackageStatus(mobilePackage.getPackageStatus())
                     .build();

        responseObserver.onNext(response);

        responseObserver.onCompleted();
    }
}
